import java.util.Arrays;

public class BinaryHeap {

    private Word[] wordArray;
    private int size;
    private int nextNode = 0;

    public BinaryHeap(Word[] array) {
        size = array.length;
        buildHeap(array);
    }

    // Problem #2 (20 pts)
    // Fill in the following method with an O(n) time algorithm
    // that builds an n element complete binary heap.
    // Note: You are allowed to add helper methods.
    public void buildHeap(Word[] array) {
        wordArray = new Word[size+1];
        System.arraycopy(array,0,wordArray,1,array.length);
        MaxHeapify(array);

    }
    public void MaxHeapify(Word[] arr){
        for (int i = size/2; i < size; i++) {
            MaxHeapify(i);
        }
    }
    private void MaxHeapify(int index){
        if(index <= 0){
           //System.out.println("index less then 1");
        }else if(isLeaf(index)){
            MaxHeapify(parentOf(index));
        }else if(wordArray[index].compareTo(wordArray[leftOf(index)]) < 0 || // check if index is smaller then down winds
            wordArray[index].compareTo(wordArray[rightOf(index)]) < 0){

            if(wordArray[index].compareTo(wordArray[leftOf(index)]) < 0 ){
                Selector.swap(wordArray, index, leftOf(index));
                MaxHeapify(leftOf(index));
                return;
            }else{
                Selector.swap(wordArray, index, rightOf(index));
                MaxHeapify(rightOf(index));
                return;
            }
        }else{
            MaxHeapify(parentOf(index));
        }
    }

    // Problem #3 (15 pts)
    // Fill in the following method with an O(log(n)) time algorithm
    // that removes the root element, restores the heap structure,
    // and finally returns the removed root element.
    // Note: You are allowed to add helper methods.
    public Word removeMax() {
        Word retW = wordArray[1];
        Selector.swap(wordArray,1,size-1);
        size--;
        Word[] arr = new Word[size+1];
        System.arraycopy(wordArray,0,arr,0,arr.length);
        wordArray = arr;
        MaxHeapify(wordArray);
        return retW;
    }

    public void SinkNull(int i){
        if(!isLeaf(i)){
            if (wordArray[leftOf(i)].compareTo(wordArray[rightOf(i)]) > 0){
                Selector.swap(wordArray,i, leftOf(i));
            }else {
                Selector.swap(wordArray,i, rightOf(i));
            }
        }
    }

    private int leftOf(int index){
        int out = index * 2;
        if(out > 0 && out < size){
            return out;
        }else return -1;
    }
    private int rightOf(int index){
        int out = (index * 2) + 1;
        if(out > 0 && out < size){
            return out;
        }else return -1;
    }

    private int parentOf(int index){
        if(index == 1){
            return 0;
        }
        return index/2;
    }

    private boolean isLeaf(int index){
        return index >= (size/2) && index <= size;
    }

    @Override
    public String toString() {
        return Arrays.toString(wordArray);
    }

}
