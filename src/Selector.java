import java.util.concurrent.TimeUnit;

@SuppressWarnings("ALL")
public class Selector {

    public static boolean oneNdone = true;
    public static int Comparisons = 0, Swaps = 0;
    public static long DeltaTime = 0;

    public static String StatRecord = "";
    // This is a helper method
    public static void swap(Word[] array, int i, int j) {
        Word temp = array[i];
        array[i] = array[j];
        array[j] = temp;
        Swaps++;
    }

    public static int Partition(Word[] arr, int leftHead, int rightHead, int pivot){

        Word pivotWord = arr[pivot];
        swap(arr, leftHead, pivot);
        pivot = leftHead;
        for (int i = pivot+1; i <= rightHead; i++) {
            if (pivotWord.compareTo(arr[i]) <= 0){
                for (int j = i; j > pivot; j--){
                    swap(arr,j,j-1);
                }
                pivot++;
            }
        }
        return pivot;
    }

    // Problem #1 (5 pts)
    // Fill in the following method with an O(n*k) time algorithm
    // that returns the k largest elements of array in order from
    // largest to smallest.
    // Note: This should return an array with k elements.
    // Hint: Your approach should be similar to selection sort.
    public static Word[] simpleSelect(Word[] array, int k) {
        Swaps = 0;
        Comparisons = 0;
        DeltaTime = System.nanoTime();

        int cap = array.length-1;
        while((cap = Partition(array,0,array.length-1,k-1)) != k-1 );
        for (int i = 0; i <= k; i++){
            Partition(array,k-i,k,k-i);
        }


        Word[] output = new Word[k];

        System.arraycopy(array, 0, output, 0, k);
        DeltaTime = System.nanoTime() - DeltaTime;
        String formatted = String.format("\nChart for K=%d\n" +
                "+--------------------------------------------+\n" +
                "|     Time     |  Comparisons  |    Swaps    |\n" +
                "|  Nanosecond  |  CompTo only  |  FuncCalls  |\n" +
                "|==============|===============|=============|\n" +
                "| %-12d | %-13d | %-11d |\n" +
                "+--------------------------------------------+",k,DeltaTime, Comparisons, Swaps);
        StatRecord +=""+k+","+ DeltaTime +","+Comparisons+","+Swaps+"\n";
        System.out.println(formatted);
        return output;
    }
    // Problem #4 (5 pts)
    // Fill in the following method with an O(n + klog(n)) time algorithm
    // that returns the k largest elements of array in order from
    // largest to smallest.
    // Note: This should return an array with k elements.
    // Hint: Your approach should use a BinaryHeap.
    public static Word[] heapSelect(Word[] array, int k) {
        BinaryHeap binaryHeap = new BinaryHeap(array);
        Word[] output = new Word[k];

        for(int i = 0; i < k; i++){
            output[i] = binaryHeap.removeMax();
        }

        return output;
    }

}
